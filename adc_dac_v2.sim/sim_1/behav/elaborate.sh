#!/bin/bash -f
xv_path="/opt/Xilinx/Vivado/2016.4"
ExecStep()
{
"$@"
RETVAL=$?
if [ $RETVAL -ne 0 ]
then
exit $RETVAL
fi
}
ExecStep $xv_path/bin/xelab -wto bf448ce061e54fcda12b62dfac0d3088 -m64 --debug typical --relax --mt 8 -L xil_defaultlib -L secureip --snapshot tb_adc_behav xil_defaultlib.tb_adc -log elaborate.log
